var Request = function () {

};

$.extend(Request.prototype, {

    spinnerHtml: function () {
        return "<img class='spinner' width='200' height='200' src='/images/loading_spinner.gif'>";
    },

    buildRaceTable: function (data) {

        if(data.result.length < 1) {
            return 'No results found.';
        }

        var table = '<table class="table-races">';
        table = table + '<thead><tr>';
        table = table + '<th></th>';
        table = table + '<th></th>';

        var hashList = {};

        /* table header */
        $.each(data.hashes, function( index, value ) {
            table = table + '<th class="rotate"><div><span>' + index + '</span></div></th>';

            hashList[value] = -1;
        });
        table = table + '</thead><tbody>';

        localStorage.setItem('hashList', JSON.stringify(hashList));

        $.each(data.result, function( index, value ) {

            d = new Date(value.startDate);
            var datestring = d.getDate()  + "-" + (d.getMonth()+1) + "-" + d.getFullYear();

            table = table + '<tr><th>' + datestring + '</th>';
            table = table + '<th>' + value.name.trunc(50) + '</th>';

            var personResult = JSON.parse(localStorage.getItem('hashList')); //copy hashlist

            $.each(value.categories, function( catKey, catVal ) {
                $.each(catVal.participants, function( riderKey, riderVal ) {
                    if(parseInt(riderVal.dnf) == 1) {
                        personResult[riderVal.hash] = -3;
                    } else if(parseInt(riderVal.dns) == 1) {
                        personResult[riderVal.hash] = -2;
                    } else if(parseInt(riderVal.dq) == 1) {
                        personResult[riderVal.hash] = -4;
                    } else if(parseInt(riderVal.position) > 0) {
                        personResult[riderVal.hash] = parseInt(riderVal.position);
                    } else {
                        personResult[riderVal.hash] = 0;
                    }
                });
            });

            $.each(personResult, function( resultKey, resultVal ) {
                // 0 = op deelnerslijst
                // -1 = niet op lijst
                // -2 = DNS
                // -3 = DNF
                // -4 = DQ

                   if(resultVal == -1) {
                       table = table + '<td>&nbsp;</td>';
                   } else if(resultVal == 0) {
                       table = table + '<td>X</td>';
                   } else if(resultVal == -2) {
                       table = table + '<td>DNS</td>';
                   } else if(resultVal == -3) {
                       table = table + '<td>DNF</td>';
                   } else if(resultVal == -4) {
                       table = table + '<td>DQ</td>';
                   } else {
                       table = table + '<td>'+resultVal+'</td>';
                   }
                });

                table = table + '</tr>';
        });

        table = table + '</tbody></table>';

        return table;
    },

    getRaces: function (year, month, token) {
        var self = this;

        $('#raceData').html(self.spinnerHtml());
        $('#filterRaces').prop('disabled', true);

        $.post( "/ajax/getRaces", { year: year, month: month, _token: token })
            .done(function( data ) {
                var json = JSON.parse(data);
                var table = self.buildRaceTable(json);
                $('#raceData').html(table);

                $('#filterRaces').prop('disabled', false);
            });
    },

});

var oRequest = new Request();

String.prototype.trunc =
    function(n){
        return this.substr(0,n-1)+(this.length>n?'&hellip;':'');
    };

$(function () {
    oRequest.getRaces($('#txtYear').val(), $('#txtMonth').val(), $('#token').val());

   $('#filterRaces').on('click', function () {
       oRequest.getRaces($('#txtYear').val(), $('#txtMonth').val(), $('#token').val());
   }) ;
});
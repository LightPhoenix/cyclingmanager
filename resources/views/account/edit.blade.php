@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Attach Cyclist</div>

                    <div class="panel-body">

                        {{ Form::open(['route' => 'account.post']) }}

                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Location</th>
                                <th scope="col">Hash</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($aData as $row)
                                <tr>
                                    @if($oUser->racerHash == $row->hash)
                                        <td>{{ Form::radio('racerHash', $row->hash, true) }}</td>
                                    @else
                                        <td>{{ Form::radio('racerHash', $row->hash, false) }}</td>
                                    @endif
                                    <td>{{ $row->name }}</td>
                                    <td>{{ $row->from }}</td>
                                    <td>{{ $row->hash }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>


                        <div class="form-group">
                            {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
                        </div>

                        {{ Form::close() }}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
